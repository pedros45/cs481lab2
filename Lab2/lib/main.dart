import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:url_launcher/url_launcher.dart';

void main() => runApp(MyApp());

/// This is the main application widget.
class MyApp extends StatelessWidget {
  static const String _title = 'CS481 Lab 2';

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: _title,
      theme: ThemeData(fontFamily: 'Montserrat'),
      home: MyStatefulWidget(),
    );
  }
}

class MyStatefulWidget extends StatefulWidget {
  MyStatefulWidget({Key key}) : super(key: key);

  @override
  _MyStatefulWidgetState createState() => _MyStatefulWidgetState();
}

class _MyStatefulWidgetState extends State<MyStatefulWidget> {
  int _selectedIndex = 0;
  static const TextStyle optionStyle =
      TextStyle(fontSize: 30, fontWeight: FontWeight.bold);

  /*
   * This list should hold the widgets for the pages that we will be adding
   * the modal and persistent sheets onto!
   */
  static List<Widget> _widgetOptions = <Widget>[
    MyHomePage(),
    MajorsPage(),
    //ModalWidget(),
    AdmissionsPage(),
    //PersistentWidget(),
  ];

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('CS481 Lab 2'),
      ),
      body: Center(
        //This is where the pages are referenced when the button is clocked
        child: _widgetOptions.elementAt(_selectedIndex), ),
      //button used for the 'Follow Us' page

      floatingActionButton: FloatingActionButton.extended(
        //backgroundColor: Colors.black,
        //foregroundColor: Colors.white,
        onPressed: () {
          // Respond to button press
          _followUsSheet();
        },
        icon: Icon(Icons.add),
        label: Text('Follow Us'),

      ),

      //code for the bottom navigation bar
      bottomNavigationBar: BottomNavigationBar(
        //sets the background color of the bottom navigation bar
        backgroundColor: Colors.blue,
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            title: Text('Home'),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.school),
            title: Text('Majors'),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.business),
            title: Text('Admissions'),
          ),
        ],
        currentIndex: _selectedIndex,
        //changes the color of the icons when they are selected
        selectedItemColor: Colors.grey[800],
        //changes the color of the icons when they are no selected
        unselectedItemColor: Colors.white,
        onTap: _onItemTapped,
      ),
    );
  }

  // Stylization of the Modal Sheet
  void _followUsSheet() {
    showModalBottomSheet(
        context: context,
        builder: (context) {
          return Container(
            color: Color(0xFF737373),
            //used to change the height of the modal bottom sheet 'Follow Us'
            height: 340,
            child: Container(
              child: _buildfollowUsSheet(),
              decoration: BoxDecoration(
                color: Theme.of(context).canvasColor,
                borderRadius: BorderRadius.only(
                  topLeft: const Radius.circular(20),
                  topRight: const Radius.circular(20),
                ),
              ),
            ),
          );
        });
  }

  // Content that is within Modal Sheet
  ListView _buildfollowUsSheet() {
    return ListView(
      children: <Widget>[
        SizedBox(height: 20),
        Container(
          child: Center(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text('Follow Us',
                  style: TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.bold,
                  ),
                ),
              ],
            ),
          ), //Center
        ),

        ListTile(
          leading: Image.asset("images/instagram.png",
            height: 25,
            width: 25,
          ),
          title: Text('Instagram'),
          onTap: () => _launchInstagram(),
        ),
        ListTile(
          leading: Image.asset("images/twitter.png",
            height: 25,
            width: 25,
          ),
          title: Text('Twitter'),
          onTap: () => _launchTwitter(),
        ),
        ListTile(
          leading: Image.asset("images/facebook.png",
            height: 25,
            width: 25,
          ),
          title: Text('Facebook'),
          onTap: () => _launchFacebook(),
        ),
        ListTile(
          leading: Image.asset("images/youtube.png",
            height: 25,
            width: 25,
          ),
          title: Text('YouTube'),
          onTap: () => _launchYoutube(),
        ),
        ListTile(
          leading: Image.asset("images/linkedin.png",
            height: 25,
            width: 25,
          ),
          title: Text('LinkedIn'),
          onTap: () => _launchLinkedin(),
        ),
      ],
    );
  }

  // function to launch CSUSM's instagram site
  _launchInstagram() async {
    const url = 'https://www.instagram.com/csusm/';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  // function to launch CSUSM's twitter site
  _launchTwitter() async {
    const url = 'https://twitter.com/CSUSM';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  // function to launch CSUSM's facebook site
  _launchFacebook() async {
    const url = 'https://www.facebook.com/csusm';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  // function to launch CSUSM's youtube site
  _launchYoutube() async {
    const url = 'https://www.youtube.com/user/csusm';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  // function to launch CSUSM's linkedin site
  _launchLinkedin() async {
    const url = 'https://www.linkedin.com/school/csusm/';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }
}

class MyHomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView (
        children: [
          //invisible spacing box
          SizedBox(height: 20),

          Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Flexible(
                    child: Text(
                      'CALIFORNIA STATE UNIVERSITY SAN MARCOS',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                ],
              ),
          ),

          //invisible spacing box
          SizedBox(height: 20),

          //colored spacing box
          Container(
            height: 20,
            color: Colors.blue,
          ),

          Image.asset(
            'images/kelloggplaza.png',
            //width: 600,
            //height: 200,
            fit: BoxFit.cover,
          ),

          //colored spacing box
          Container(
            height: 20,
            color: Colors.blue,
          ),

          //invisible spacing box
          SizedBox(height: 20),

          Container(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Flexible(
                  child: Text(
                    'ABOUT US',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
              ],
            ),
          ),

          Padding(
            padding: const EdgeInsets.all(18.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Flexible(
                        child: Text(
                          ('Building on an innovative 30-year history, '
                              'California State University San Marcos is a '
                              'forward-focused institution, dedicated to '
                              'preparing future leaders, building great '
                              'communities and solving critical issues. Located '
                              'on a 304-acre hillside overlooking the city of '
                              'San Marcos, the University is just a short '
                              'distance from some of Southern California’s best '
                              'beaches and an hour from the U.S.-Mexico border. '
                              'CSUSM enrollment is over 16,000 and the '
                              'university is fully accredited by the Western '
                              'Association of Schools and Colleges.\n\n'
                              'California State University San Marcos is located'
                              ' in northern San Diego County and offers '
                              'beautiful beaches, an hour drive to the mountains'
                              ' and desert, a downtown nightlife, world-class '
                              'shopping, and more all within a short trip from '
                              'campus!'),
                        ),
                      ),

                    ],
                  ),
                ),
              ],
            ),
          ),

          //invisible spacing box
          SizedBox(height: 20),

          //colored spacing box
          Container(
            height: 20,
            color: Colors.blue,
          ),

          Image.asset(
            'images/physics.jpg',
            //width: 600,
            //height: 200,
            fit: BoxFit.cover,
          ),

          //colored spacing box
          Container(
            height: 20,
            color: Colors.blue,
          ),

          //invisible spacing box
          SizedBox(height: 20),

          Container(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Flexible(
                  child: Text(
                    'Mission',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
              ],
            ),
          ),

          Padding(
            padding: const EdgeInsets.all(18.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Flexible(
                        child: Text(
                          ('California State University San Marcos focuses on '
                              'the student as an active participant in the '
                              'learning process. As a Carnegie classified '
                              '"community engaged" university, CSUSM students '
                              'work closely with a faculty whose commitment to '
                              'sustained excellence in teaching, research, and '
                              'community partnership enhances student learning. '
                              'The university offers rigorous undergraduate and '
                              'graduate programs distinguished by exemplary '
                              'teaching, innovative curricula, and the '
                              'application of new technologies. CSUSM provides a'
                              ' range of services that respond to the needs of a'
                              ' student body with diverse backgrounds, expanding'
                              ' student access to an excellent and affordable '
                              'education. As a public university, CSUSM grounds '
                              'its mission in the public trust, alignment with '
                              'regional needs, and sustained enrichment of the '
                              'intellectual, civic, economic, and cultural life '
                              'of our region and state.'),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),

          //invisible spacing box
          SizedBox(height: 20),

          //colored spacing box
          Container(
            height: 20,
            color: Colors.blue,
          ),

          Image.asset(
            'images/winterintersession.jpg',
            //width: 600,
            //height: 200,
            fit: BoxFit.cover,
          ),

          //colored spacing box
          Container(
            height: 20,
            color: Colors.blue,
          ),

          //invisible spacing box
          SizedBox(height: 20),

          Container(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Flexible(
                  child: Text(
                    'Vision',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
              ],
            ),
          ),

          Padding(
            padding: const EdgeInsets.all(18.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Flexible(
                        child: Text(
                          ('California State University San Marcos will become a'
                              ' distinctive public university known for academic'
                              ' excellence, service to the community, and '
                              'innovation in higher education. In its teaching '
                              'and student services, CSUSM will combine the '
                              'academic strengths of a large university with the'
                              ' personal interaction characteristic of smaller '
                              'institutions. Students will select from a growing'
                              ' array of specialized programs responsive to '
                              'state and regional needs. Our curriculum will '
                              'emphasize a strong foundation in the liberal arts'
                              ' and sciences while it provides the knowledge, '
                              'skills, competencies and experiences needed in a '
                              'global society experiencing accelerated '
                              'technological, social, and environmental change. '
                              'A faculty of active scholars and artists will '
                              'foster student learning through teaching that '
                              'reflects ongoing discovery and experimentation, '
                              'and that makes a difference in the life of our '
                              'communities. CSUSM will celebrate and capitalize '
                              'on its diversity to form a learning community '
                              'committed to this shared vision.'),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),

          //invisible spacing box
          SizedBox(height: 60),

        ], // Children
      ),
    );
  }
}

class MajorsPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView (
        children: [
          Image.asset(
            'images/threestudentsonlawn.jpeg',
            fit: BoxFit.cover,
          ),

          //colored spacing box
          Container(
            height: 20,
            color: Colors.blue,
          ),

          //invisible spacing box
          SizedBox(height: 20),

          //title of the page
          Container(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Flexible(
                  child: Text(
                    'MAJORS AND PROGRAMS',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
              ],
            ),
          ),

          //top text of the page
          Padding(
            padding: const EdgeInsets.all(18.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Flexible(
                        child: Text(
                          ('All CSUSM students must declare a primary major before they reach 60 units.'),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),

          //start of the list of majors
          Divider(
            color: Colors.blue,
            indent: 20,
            endIndent: 20,
          ),

          Padding(
            padding: EdgeInsets.only(left: 27.0, right: 27.0),
            child:
              Text(
                'American Indian Studies, B.A.',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: Colors.blue,
                ),
              ),
          ),

          Divider(
            color: Colors.blue,
            indent: 20,
            endIndent: 20,
          ),

          Padding(
            padding: EdgeInsets.only(left: 27.0, right: 27.0),
            child:
            Text(
              'Anthropolgy, B.A.',
              style: TextStyle(
                fontWeight: FontWeight.bold,
                color: Colors.blue,
              ),
            ),
          ),

          Padding(
            padding: EdgeInsets.only(left: 27.0, right: 27.0),
            child:
            Text('> Indigenous Anthropology\n> Medical Anthropology'),
          ),

          Divider(
            color: Colors.blue,
            indent: 20,
            endIndent: 20,
          ),

          Padding(
            padding: EdgeInsets.only(left: 27.0, right: 27.0),
            child:
            Text(
              'Applied Physics, B.S.',
              style: TextStyle(
                fontWeight: FontWeight.bold,
                color: Colors.blue,
              ),
            ),
          ),

          Padding(
            padding: EdgeInsets.only(left: 27.0, right: 27.0),
            child:
            Text('> Applied Electronics\n> Applied Physics'),
          ),

          Divider(
            color: Colors.blue,
            indent: 20,
            endIndent: 20,
          ),

          Padding(
            padding: EdgeInsets.only(left: 27.0, right: 27.0),
            child:
            Text(
              'Art, Media, & Design, B.A.',
              style: TextStyle(
                fontWeight: FontWeight.bold,
                color: Colors.blue,
              ),
            ),
          ),

          Padding(
            padding: EdgeInsets.only(left: 27.0, right: 27.0),
            child:
            Text('> Art & Visual Culture\n> Digital & Media Arts'),
          ),

          Divider(
            color: Colors.blue,
            indent: 20,
            endIndent: 20,
          ),

          Padding(
            padding: EdgeInsets.only(left: 27.0, right: 27.0),
            child:
            Text(
              'Biochemistry, B.S.',
              style: TextStyle(
                fontWeight: FontWeight.bold,
                color: Colors.blue,
              ),
            ),
          ),

          Divider(
            color: Colors.blue,
            indent: 20,
            endIndent: 20,
          ),

          Padding(
            padding: EdgeInsets.only(left: 27.0, right: 27.0),
            child:
            Text(
              'Biological Sciences, B.S., M.S.',
              style: TextStyle(
                fontWeight: FontWeight.bold,
                color: Colors.blue,
              ),
            ),
          ),

          Padding(
            padding: EdgeInsets.only(left: 27.0, right: 27.0),
            child:
            Text('> Ecology\n> General Biology\n> Molecular and Cellular Biology\n> Physiology'),
          ),

          Divider(
            color: Colors.blue,
            indent: 20,
            endIndent: 20,
          ),

          Padding(
            padding: EdgeInsets.only(left: 27.0, right: 27.0),
            child:
            Text(
              'Biotechnology, B.S.',
              style: TextStyle(
                fontWeight: FontWeight.bold,
                color: Colors.blue,
              ),
            ),
          ),

          Divider(
            color: Colors.blue,
            indent: 20,
            endIndent: 20,
          ),

          Padding(
            padding: EdgeInsets.only(left: 27.0, right: 27.0),
            child:
            Text(
              'Buisiness Administration, B.S., M.B.A.',
              style: TextStyle(
                fontWeight: FontWeight.bold,
                color: Colors.blue,
              ),
            ),
          ),

          Padding(
            padding: EdgeInsets.only(left: 27.0, right: 27.0),
            child:
            Text('> Accountancy\n'
                '> Finance\n'
                '> Global Business Management\n'
                '> Global Supply Chain Management\n'
                '> Management\n'
                '> Management Information Systems\n'
                '> Marketing'),
          ),

          Divider(
            color: Colors.blue,
            indent: 20,
            endIndent: 20,
          ),

          Padding(
            padding: EdgeInsets.only(left: 27.0, right: 27.0),
            child:
            Text(
              'Chemistry, B.S.',
              style: TextStyle(
                fontWeight: FontWeight.bold,
                color: Colors.blue,
              ),
            ),
          ),

          Padding(
            padding: EdgeInsets.only(left: 27.0, right: 27.0),
            child:
            Text('> Chemistry\n'
                '> Science Education\n'
                '> Biochemistry'),
          ),

          Divider(
            color: Colors.blue,
            indent: 20,
            endIndent: 20,
          ),

          Padding(
            padding: EdgeInsets.only(left: 27.0, right: 27.0),
            child:
            Text(
              'Child and Adolescent Development, B.A.',
              style: TextStyle(
                fontWeight: FontWeight.bold,
                color: Colors.blue,
              ),
            ),
          ),

          Divider(
            color: Colors.blue,
            indent: 20,
            endIndent: 20,
          ),

          Padding(
            padding: EdgeInsets.only(left: 27.0, right: 27.0),
            child:
            Text(
              'Communication, B.A.',
              style: TextStyle(
                fontWeight: FontWeight.bold,
                color: Colors.blue,
              ),
            ),
          ),

          Divider(
            color: Colors.blue,
            indent: 20,
            endIndent: 20,
          ),

          Padding(
            padding: EdgeInsets.only(left: 27.0, right: 27.0),
            child:
            Text(
              'Computer Science, B.S., M.S.',
              style: TextStyle(
                fontWeight: FontWeight.bold,
                color: Colors.blue,
              ),
            ),
          ),

          Padding(
            padding: EdgeInsets.only(left: 27.0, right: 27.0),
            child:
            Text('> Computer Science\n'
                '> Computer Information Systems'),
          ),

          Divider(
            color: Colors.blue,
            indent: 20,
            endIndent: 20,
          ),

          Padding(
            padding: EdgeInsets.only(left: 27.0, right: 27.0),
            child:
            Text(
              'Criminology and Justice Studies, B.A.',
              style: TextStyle(
                fontWeight: FontWeight.bold,
                color: Colors.blue,
              ),
            ),
          ),

          Divider(
            color: Colors.blue,
            indent: 20,
            endIndent: 20,
          ),

          Padding(
            padding: EdgeInsets.only(left: 27.0, right: 27.0),
            child:
            Text(
              'Economics, B.A.',
              style: TextStyle(
                fontWeight: FontWeight.bold,
                color: Colors.blue,
              ),
            ),
          ),

          Divider(
            color: Colors.blue,
            indent: 20,
            endIndent: 20,
          ),

          Padding(
            padding: EdgeInsets.only(left: 27.0, right: 27.0),
            child:
            Text(
              'Education, M.A.',
              style: TextStyle(
                fontWeight: FontWeight.bold,
                color: Colors.blue,
              ),
            ),
          ),

          Divider(
            color: Colors.blue,
            indent: 20,
            endIndent: 20,
          ),

          Padding(
            padding: EdgeInsets.only(left: 27.0, right: 27.0),
            child:
            Text(
              'Engineering, B.S.',
              style: TextStyle(
                fontWeight: FontWeight.bold,
                color: Colors.blue,
              ),
            ),
          ),

          Padding(
            padding: EdgeInsets.only(left: 27.0, right: 27.0),
            child:
            Text('> Electrical\n'
                '> Software'),
          ),

          Divider(
            color: Colors.blue,
            indent: 20,
            endIndent: 20,
          ),

          Padding(
            padding: EdgeInsets.only(left: 27.0, right: 27.0),
            child:
            Text(
              'Environmental Studies, B.A.',
              style: TextStyle(
                fontWeight: FontWeight.bold,
                color: Colors.blue,
              ),
            ),
          ),

          Divider(
            color: Colors.blue,
            indent: 20,
            endIndent: 20,
          ),

          Padding(
            padding: EdgeInsets.only(left: 27.0, right: 27.0),
            child:
            Text(
              'Ethnic Studies, B.A.',
              style: TextStyle(
                fontWeight: FontWeight.bold,
                color: Colors.blue,
              ),
            ),
          ),

          Divider(
            color: Colors.blue,
            indent: 20,
            endIndent: 20,
          ),

          Padding(
            padding: EdgeInsets.only(left: 27.0, right: 27.0),
            child:
            Text(
              'Global Studies, B.A.',
              style: TextStyle(
                fontWeight: FontWeight.bold,
                color: Colors.blue,
              ),
            ),
          ),

          Divider(
            color: Colors.blue,
            indent: 20,
            endIndent: 20,
          ),

          Padding(
            padding: EdgeInsets.only(left: 27.0, right: 27.0),
            child:
            Text(
              'History, B.A., M.A.',
              style: TextStyle(
                fontWeight: FontWeight.bold,
                color: Colors.blue,
              ),
            ),
          ),

          Divider(
            color: Colors.blue,
            indent: 20,
            endIndent: 20,
          ),

          Padding(
            padding: EdgeInsets.only(left: 27.0, right: 27.0),
            child:
            Text(
              'Human Development, B.A.',
              style: TextStyle(
                fontWeight: FontWeight.bold,
                color: Colors.blue,
              ),
            ),
          ),

          Padding(
            padding: EdgeInsets.only(left: 27.0, right: 27.0),
            child:
            Text('> Counseling Services\n'
                '> Health Services'),
          ),

          Divider(
            color: Colors.blue,
            indent: 20,
            endIndent: 20,
          ),

          Padding(
            padding: EdgeInsets.only(left: 27.0, right: 27.0),
            child:
            Text(
              'Kinesiology, B.S., M.S.',
              style: TextStyle(
                fontWeight: FontWeight.bold,
                color: Colors.blue,
              ),
            ),
          ),

          Padding(
            padding: EdgeInsets.only(left: 27.0, right: 27.0),
            child:
            Text('> Health Science\n'
                '> Movement Science'),
          ),

          Divider(
            color: Colors.blue,
            indent: 20,
            endIndent: 20,
          ),

          Padding(
            padding: EdgeInsets.only(left: 27.0, right: 27.0),
            child:
            Text(
              'Liberal Studies, B.A.',
              style: TextStyle(
                fontWeight: FontWeight.bold,
                color: Colors.blue,
              ),
            ),
          ),

          Padding(
            padding: EdgeInsets.only(left: 27.0, right: 27.0),
            child:
            Text('> Border Studies\n'
                '> Elementary Subject Matter Preparation'),
          ),

          Divider(
            color: Colors.blue,
            indent: 20,
            endIndent: 20,
          ),

          Padding(
            padding: EdgeInsets.only(left: 27.0, right: 27.0),
            child:
            Text(
              'Literature and Writing Studies, B.A., M.A.',
              style: TextStyle(
                fontWeight: FontWeight.bold,
                color: Colors.blue,
              ),
            ),
          ),

          Divider(
            color: Colors.blue,
            indent: 20,
            endIndent: 20,
          ),

          Padding(
            padding: EdgeInsets.only(left: 27.0, right: 27.0),
            child:
            Text(
              'Mathematics, B.S., M.S.',
              style: TextStyle(
                fontWeight: FontWeight.bold,
                color: Colors.blue,
              ),
            ),
          ),

          Divider(
            color: Colors.blue,
            indent: 20,
            endIndent: 20,
          ),

          Padding(
            padding: EdgeInsets.only(left: 27.0, right: 27.0),
            child:
            Text(
              'Media Studies, B.A.',
              style: TextStyle(
                fontWeight: FontWeight.bold,
                color: Colors.blue,
              ),
            ),
          ),

          Divider(
            color: Colors.blue,
            indent: 20,
            endIndent: 20,
          ),

          Padding(
            padding: EdgeInsets.only(left: 27.0, right: 27.0),
            child:
            Text(
              'Music, B.A.',
              style: TextStyle(
                fontWeight: FontWeight.bold,
                color: Colors.blue,
              ),
            ),
          ),

          Divider(
            color: Colors.blue,
            indent: 20,
            endIndent: 20,
          ),

          Padding(
            padding: EdgeInsets.only(left: 27.0, right: 27.0),
            child:
            Text(
              'Nursing, B.S.',
              style: TextStyle(
                fontWeight: FontWeight.bold,
                color: Colors.blue,
              ),
            ),
          ),

          Padding(
            padding: EdgeInsets.only(left: 27.0, right: 27.0),
            child:
            Text('> Generic/ Basic BSN\n'
                '> Entry Level Baccalaureate\n'
                '> RN-to-BSN'),
          ),

          Divider(
            color: Colors.blue,
            indent: 20,
            endIndent: 20,
          ),

          Padding(
            padding: EdgeInsets.only(left: 27.0, right: 27.0),
            child:
            Text(
              'Political Science, B.A.',
              style: TextStyle(
                fontWeight: FontWeight.bold,
                color: Colors.blue,
              ),
            ),
          ),

          Padding(
            padding: EdgeInsets.only(left: 27.0, right: 27.0),
            child:
            Text('> Generic Concentration\n'
                '> Global Concentration'),
          ),

          Divider(
            color: Colors.blue,
            indent: 20,
            endIndent: 20,
          ),

          Padding(
            padding: EdgeInsets.only(left: 27.0, right: 27.0),
            child:
            Text(
              'Psychology Sciences, B.A., M.A.',
              style: TextStyle(
                fontWeight: FontWeight.bold,
                color: Colors.blue,
              ),
            ),
          ),

          Divider(
            color: Colors.blue,
            indent: 20,
            endIndent: 20,
          ),

          Padding(
            padding: EdgeInsets.only(left: 27.0, right: 27.0),
            child:
            Text(
              'Social Sciences, B.A.',
              style: TextStyle(
                fontWeight: FontWeight.bold,
                color: Colors.blue,
              ),
            ),
          ),

          Divider(
            color: Colors.blue,
            indent: 20,
            endIndent: 20,
          ),

          Padding(
            padding: EdgeInsets.only(left: 27.0, right: 27.0),
            child:
            Text(
              'Sociology, B.A.',
              style: TextStyle(
                fontWeight: FontWeight.bold,
                color: Colors.blue,
              ),
            ),
          ),

          Padding(
            padding: EdgeInsets.only(left: 27.0, right: 27.0),
            child:
            Text('> Aging and the Life Course\n'
                '> Children, Youth, and Families\n'
                '> Critical Race Studies\n'
                '> Health, Education, and Welfare'),
          ),

          Divider(
            color: Colors.blue,
            indent: 20,
            endIndent: 20,
          ),

          Padding(
            padding: EdgeInsets.only(left: 27.0, right: 27.0),
            child:
            Text(
              'Sociological Practice, M.A.',
              style: TextStyle(
                fontWeight: FontWeight.bold,
                color: Colors.blue,
              ),
            ),
          ),

          Divider(
            color: Colors.blue,
            indent: 20,
            endIndent: 20,
          ),

          Padding(
            padding: EdgeInsets.only(left: 27.0, right: 27.0),
            child:
            Text(
              'Spanish, B.A., M.A.',
              style: TextStyle(
                fontWeight: FontWeight.bold,
                color: Colors.blue,
              ),
            ),
          ),

          Padding(
            padding: EdgeInsets.only(left: 27.0, right: 27.0),
            child:
            Text('> General\n'
                '> Language and Culture\n'
                '> Literature\n'
                '> Professions'),
          ),

          Divider(
            color: Colors.blue,
            indent: 20,
            endIndent: 20,
          ),

          Padding(
            padding: EdgeInsets.only(left: 27.0, right: 27.0),
            child:
            Text(
              'Special Major, B.A.',
              style: TextStyle(
                fontWeight: FontWeight.bold,
                color: Colors.black,
              ),
            ),
          ),

          Divider(
            color: Colors.blue,
            indent: 20,
            endIndent: 20,
          ),

          Padding(
            padding: EdgeInsets.only(left: 27.0, right: 27.0),
            child:
            Text(
              'Theatre, B.A.',
              style: TextStyle(
                fontWeight: FontWeight.bold,
                color: Colors.blue,
              ),
            ),
          ),

          Divider(
            color: Colors.blue,
            indent: 20,
            endIndent: 20,
          ),

          Padding(
            padding: EdgeInsets.only(left: 27.0, right: 27.0),
            child:
            Text(
              'Undeclared',
              style: TextStyle(
                fontWeight: FontWeight.bold,
                color: Colors.black,
              ),
            ),
          ),

          Divider(
            color: Colors.blue,
            indent: 20,
            endIndent: 20,
          ),

          Padding(
            padding: EdgeInsets.only(left: 27.0, right: 27.0),
            child:
            Text(
              'Womens Gender and Sexuality Studies, B.A.',
              style: TextStyle(
                fontWeight: FontWeight.bold,
                color: Colors.blue,
              ),
            ),
          ),

          Divider(
            color: Colors.blue,
            indent: 20,
            endIndent: 20,
          ),

          //invisible spacing box
          SizedBox(height: 80),

        ], // Children
      ),
    );
  }
}

class AdmissionsPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView (
        children: [
          Image.asset(
            'images/viewfromsteps.jpg',
            fit: BoxFit.cover,
          ),

          //colored spacing box
          Container(
            height: 20,
            color: Colors.blue,
          ),

          //invisible spacing box
          SizedBox(height: 20),

          //title of the page
          Container(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Flexible(
                  child: Text(
                    'CONTACT ADMISSIONS',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontFamily: 'Montserrat',
                      fontSize: 16,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
              ],
            ),
          ),

          //top text of the page
          Padding(
            padding: const EdgeInsets.only(top: 18.0, left: 18.0, right: 18.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Flexible(
                        child: Text(
                          ('Telephone'),
                          style: TextStyle(
                            fontFamily: 'Montserrat',
                            fontSize: 16,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),

          //start of the list of majors
          Divider(
            color: Colors.black,
            indent: 20,
            endIndent: 20,
          ),

          Padding(
            padding: EdgeInsets.only(left: 27.0, right: 27.0),
            child:
            Text(
              'Office of Admissions: (760) 750-4848 - select option 0\n'
                  'Office of Recruitment & Outreach: (760) 750-4870\n'
                  'Campus Tours: (760) 750-4830\n\n'
                  'Our phone lines are open Monday-Thursday 9am-2pm.',
              style: TextStyle(
                color: Colors.black,
                fontSize: 12,
              ),
            ),
          ),

          //top text of the page
          Padding(
            padding: const EdgeInsets.only(top: 18.0, left: 18.0, right: 18.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Flexible(
                        child: Text(
                          ('Fax'),
                          style: TextStyle(
                            fontFamily: 'Montserrat',
                            fontSize: 16,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),

          //start of the list of majors
          Divider(
            color: Colors.black,
            indent: 20,
            endIndent: 20,
          ),

          Padding(
            padding: EdgeInsets.only(left: 27.0, right: 27.0),
            child:
            Text(
              'Admissions Fax: (760) 750-3248\n'
                  'Recruitment & Outreach Fax: (760) 750-3089\n\n'
                  'Due to the campus closure to in-person services, faxes are '
                  'not currently being monitored. Please contact us through our '
                  'contact form and we will respond as quickly as possible.',
              style: TextStyle(
                color: Colors.black,
                fontSize: 12,
              ),
            ),
          ),

          //top text of the page
          Padding(
            padding: const EdgeInsets.only(top: 18.0, left: 18.0, right: 18.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Flexible(
                        child: Text(
                          ('Mailing Address'),
                          style: TextStyle(
                            fontFamily: 'Montserrat',
                            fontSize: 16,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),

          //start of the list of majors
          Divider(
            color: Colors.black,
            indent: 20,
            endIndent: 20,
          ),

          Padding(
            padding: EdgeInsets.only(left: 27.0, right: 27.0),
            child:
            Text(
              'Office of Admissions\n'
                  'California State University San Marcos\n'
                  '333 South Twin Oaks Valley Road\n'
                  'San Marcos, CA 92096-0001\n\n'
                  'Office of Recruitment & Outreach\n'
                  'California State University San Marcos\n'
                  '333 South Twin Oaks Valley Road\n'
                  'San Marcos, CA 92096-0001',
              style: TextStyle(
                color: Colors.black,
                fontSize: 12,
              ),
            ),
          ),

          //top text of the page
          Padding(
            padding: const EdgeInsets.only(top: 18.0, left: 18.0, right: 18.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Flexible(
                        child: Text(
                          ('Location'),
                          style: TextStyle(
                            fontFamily: 'Montserrat',
                            fontSize: 16,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),

          //start of the list of majors
          Divider(
            color: Colors.black,
            indent: 20,
            endIndent: 20,
          ),

          Padding(
            padding: EdgeInsets.only(left: 27.0, right: 27.0),
            child:
            Text(
              'Office of Admissions: Cougar Central, Craven Hall 3700\n'
                  'Office of Recruitment & Outreach: Craven Hall 3300\n\n'
                  'Our physical office is currently closed due to social '
                  'distancing measures.',
              style: TextStyle(
                color: Colors.black,
                fontSize: 12,
              ),
            ),
          ),

          //invisible spacing box
          SizedBox(height: 80),

        ], // Children
      ),
    );
  }
}

/*
// MODAL WIDGET
class ModalWidget extends StatefulWidget {
  @override
  _ModalState createState() => _ModalState();
}

class _ModalState extends State<ModalWidget> {
  String _selectedItem = '';

  // Build raised options button on the page and call functions
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            RaisedButton(
              child: Text('Options'),
              onPressed: () => _onButtonPressed(),
            ),
            Text(
              _selectedItem,
              style: TextStyle(fontSize: 30),
            ),
          ],
        ),
      ),
    );
  }

  // Stylization of the Modal Sheet
  void _onButtonPressed() {
    showModalBottomSheet(
        context: context,
        builder: (context) {
          return Container(
            color: Color(0xFF737373),
            height: 240,
            child: Container(
              child: _buildModalNavigationMenu(),
              decoration: BoxDecoration(
                color: Theme.of(context).canvasColor,
                borderRadius: BorderRadius.only(
                  topLeft: const Radius.circular(20),
                  topRight: const Radius.circular(20),
                ),
              ),
            ),
          );
        });
  }

  // Content that is within Modal Sheet
  Column _buildModalNavigationMenu() {
    return Column(
      children: <Widget>[
        ListTile(
          leading: Icon(Icons.local_pizza),
          title: Text('Calorie Counter'),
          onTap: () => _selectItem('Calorie Counter'),
        ),
        ListTile(
          leading: Icon(Icons.accessibility_new),
          title: Text('Workouts'),
          onTap: () => _selectItem('Workouts'),
        ),
        ListTile(
          leading: Icon(Icons.assessment),
          title: Text('Weight Loss'),
          onTap: () => _selectItem('Weight Loss'),
        ),
        ListTile(
          leading: Icon(Icons.exit_to_app),
          title: Text('Clear'),
          onTap: () => _selectItem(' '),
        ),
      ],
    );
  }

  // Function that sets selectedItem to
  void _selectItem(String name) {
    Navigator.pop(context);
    setState(() {
      _selectedItem = name;
    });
  }
}

// PERSISTENT WIDGET
class PersistentWidget extends StatefulWidget {
  @override
  _PersistentState createState() => _PersistentState();
}

class _PersistentState extends State<PersistentWidget> {
  Color color = Colors.white;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: color,
      body: Center(
        child: RaisedButton(
          child: const Text('Change Background Color'),
          onPressed: () {
            Scaffold.of(context).showBottomSheet<void>(
              (BuildContext context) {
                return Container(
                  height: 400,
                  color: Colors.blue,
                  child: Center(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        const Text('Choose Background Color: \n'),
                        RaisedButton(
                            child: const Text('White'),
                            onPressed: () =>
                                setState(() => color = Colors.white)),
                        RaisedButton(
                            child: const Text('Red'),
                            onPressed: () =>
                                setState(() => color = Colors.red)),
                        RaisedButton(
                            child: const Text('Blue'),
                            onPressed: () =>
                                setState(() => color = Colors.lightBlue)),
                        RaisedButton(
                            child: const Text('Green'),
                            onPressed: () =>
                                setState(() => color = Colors.lightGreen)),
                        RaisedButton(
                            child: const Text('Yellow'),
                            onPressed: () =>
                                setState(() => color = Colors.yellow)),
                        RaisedButton(
                            child: const Text('Orange'),
                            onPressed: () =>
                                setState(() => color = Colors.orange)),
                        RaisedButton(
                          child: const Text('Close'),
                          onPressed: () => Navigator.pop(context),
                        )
                      ],
                    ),
                  ),
                );
              },
            );
          },
        ),
      ),
    );
  }
}
*/